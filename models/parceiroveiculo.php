<?php

include_once $_SERVER['DOCUMENT_ROOT']. '/models/connection.php';

class parceiroveiculo extends connection {

    function insertParceiroVeiculo($cdparceiroveiculomodelo, $cdparceiroveiculotipo, $cdparceiro, $txplaca, $txrenavam, $txmarca_modelo, $txanofabricacao, $txanomodelo, $txcor, $vlcapacidade, $txdocumento, $txfoto1, $txfoto2, $txfoto3, $txfoto4, $flsituacao) {

        $conection = new connection();

        $hj = date("Y-m-d H:i:s");

        if ($flsituacao) {
            $dtaprovado = $hj;
        } else {
            $dtaprovado = "";
        }

        $query = "INSERT INTO parceiroveiculo "
                . "(cdparceiroveiculomodelo, cdparceiroveiculotipo, cdparceiro, txplaca, txrenavam, txmarca_modelo, txanofabricacao, txanomodelo, txcor, vlcapacidade, txdocumento, txfoto1, txfoto2, txfoto3, txfoto4, flsituacao, flexcluido) "
                . " VALUES "
                . "('$cdparceiroveiculomodelo', '$cdparceiroveiculotipo', '$cdparceiro', '$txplaca', '$txrenavam', '$txmarca_modelo', '$txanofabricacao', '$txanomodelo', '$txcor', '$vlcapacidade', '$txdocumento', '$txfoto1', '$txfoto2', '$txfoto3', '$txfoto4', '$flsituacao', 0)";
// echo $query;
        $result = $conection->query($query);
        if ($result) {
            $id = $conection->insert_id();
        } else {
            $id = 0;
        }
        return $id;
    }

    function updateParceiroVeiculo($cdparceiroveiculo, $cdparceiroveiculomodelo, $cdparceiroveiculotipo, $txplaca, $txrenavam, $txmarca_modelo, $txanofabricacao, $txanomodelo, $txcor, $vlcapacidade, $txdocumento, $txfoto1, $txfoto2, $txfoto3, $txfoto4, $flsituacao) {

        $conection = new connection();
        $query = " UPDATE parceiroveiculo SET "
                . " cdparceiroveiculomodelo = '$cdparceiroveiculomodelo', cdparceiroveiculotipo = '$cdparceiroveiculotipo', "
                . " txplaca = '$txplaca', txrenavam = '$txrenavam', "
                . " txmarca_modelo = '$txmarca_modelo', txanofabricacao = '$txanofabricacao', txanomodelo = '$txanomodelo', "
                . " txcor = '$txcor', vlcapacidade = '$vlcapacidade', txdocumento = '$txdocumento', "
                . " txfoto1 = '$txfoto1', txfoto2 = '$txfoto2', txfoto3 = '$txfoto3', txfoto4 = '$txfoto4', "
                . " flsituacao = '$flsituacao'   "
                . "WHERE cdparceiroveiculo = $cdparceiroveiculo ";

// echo $query;
        $result = $conection->query($query);

        return $result;
    }

    function updateSituacaoParceiroVeiculo($cdparceiroveiculo, $flsituacao) {
        $conection = new connection();
        $query = "UPDATE parceiroveiculo SET "
                . "flsituacao = $flsituacao "
                . "WHERE cdparceiroveiculo = $cdparceiroveiculo ";
//echo $query;
        $result = $conection->query($query);

        return $result;
    }

    function deleteParceiroVeiculo($cdparceiroveiculo) {
        $conection = new connection();
        $query = "UPDATE parceiroveiculo SET "
                . "flexcluido = 1 "
                . "WHERE cdparceiroveiculo = $cdparceiroveiculo ";

        $result = $conection->query($query);

        return $result;
    }

    function retornaParceiroVeiculo($cdparceiroveiculo) {
        $conection = new connection();
        $query = "SELECT F.*  FROM parceiroveiculo F  "
                . "WHERE  F.cdparceiroveiculo = $cdparceiroveiculo AND F.flexcluido <> 1 ";

        $result = $conection->query($query);
        $rows = $conection->fetch_array($result);
        return $rows;
    }

    function retornaQuantidadesParceiroVeiculo($busca = null) {
        $conection = new connection();
        $query = "  SELECT count(DISTINCT M.cdparceiroveiculo) as total "
                . " FROM parceiroveiculo M  "
                . " WHERE  M.flexcluido <> 1  ";

        if ($busca) {
            
        }

//        echo $query;
        $result = $conection->query($query);
        $rows = $conection->fetch_array($result);
        return $rows;
    }

    function retornaTodosParceiroVeiculo($cdparceiro, $busca, $tipo) {
        $conection = new connection();
        $query = "  SELECT M.*, T.txparceiroveiculomodelo "
                . " FROM parceiroveiculo M, parceiroveiculomodelo T "
                . " WHERE M.cdparceiroveiculomodelo = T.cdparceiroveiculomodelo AND M.flexcluido <> 1 AND cdparceiro = $cdparceiro  ";

        if ($busca) {
            
        }

        $query .= " GROUP BY M.cdparceiroveiculo ";
//        echo $query; 
        $result = $conection->query($query);
        while ($rows = $conection->fetch_array($result)) {
            $return[] = $rows;
        }
        return $return;
    }

    
 
}
