<?php

include_once $_SERVER['DOCUMENT_ROOT']. '/models/connection.php';

class parceiromotorista extends connection {

    function insertParceiroMotorista($cdparceiro, $txnome, $txrg, $txcpf, $txhabilitacao, $txdocumento, $txfoto1, $flsituacao) {

        $conection = new connection();

        $hj = date("Y-m-d H:i:s");

        if ($flsituacao) {
            $dtaprovado = $hj;
        } else {
            $dtaprovado = "";
        }

        $query = "INSERT INTO parceiromotorista "
                . "(cdparceiro, txnome, txrg, txcpf, txhabilitacao, txdocumento, txfoto1, flsituacao, flexcluido) "
                . " VALUES "
                . "('$cdparceiro', '$txnome', '$txrg', '$txcpf', '$txhabilitacao', '$txdocumento', '$txfoto1', '$flsituacao', 0)";
// echo $query;
        $result = $conection->query($query);
        if ($result) {
            $id = $conection->insert_id();
        } else {
            $id = 0;
        }
        return $id;
    }

    function updateParceiroMotorista($cdparceiromotorista, $txnome, $txrg, $txcpf, $txhabilitacao, $txdocumento, $txfoto1, $flsituacao) {

        $conection = new connection();
        $query = " UPDATE parceiromotorista SET "
                . " txnome = '$txnome', txrg = '$txrg', txcpf = '$txcpf', txhabilitacao = '$txhabilitacao', "
                . " txdocumento = '$txdocumento', txfoto1 = '$txfoto1', flsituacao = '$flsituacao'   "
                . "WHERE cdparceiromotorista = $cdparceiromotorista ";

// echo $query;
        $result = $conection->query($query);

        return $result;
    }

    function updateSituacaoParceiroMotorista($cdparceiromotorista, $flsituacao) {
        $conection = new connection();
        $query = "UPDATE parceiromotorista SET "
                . "flsituacao = $flsituacao "
                . "WHERE cdparceiromotorista = $cdparceiromotorista ";
//echo $query;
        $result = $conection->query($query);

        return $result;
    }

    function deleteParceiroMotorista($cdparceiromotorista) {
        $conection = new connection();
        $query = "UPDATE parceiromotorista SET "
                . "flexcluido = 1 "
                . "WHERE cdparceiromotorista = $cdparceiromotorista ";

        $result = $conection->query($query);

        return $result;
    }

    function retornaParceiroMotorista($cdparceiromotorista) {
        $conection = new connection();
        $query = "SELECT F.*  FROM parceiromotorista F  "
                . "WHERE  F.cdparceiromotorista = $cdparceiromotorista AND F.flexcluido <> 1 ";

        $result = $conection->query($query);
        $rows = $conection->fetch_array($result);
        return $rows;
    }

    function retornaQuantidadesParceiroMotorista($busca = null) {
        $conection = new connection();
        $query = "  SELECT count(DISTINCT M.cdparceiromotorista) as total "
                . " FROM parceiromotorista M  "
                . " WHERE  M.flexcluido <> 1  ";

        if ($busca) {
            
        }

//        echo $query;
        $result = $conection->query($query);
        $rows = $conection->fetch_array($result);
        return $rows;
    }

    function retornaTodosParceiroMotorista($busca, $tipo) {
        $conection = new connection();
        $query = "  SELECT M.* "
                . " FROM parceiromotorista M "
                . " WHERE M.flexcluido <> 1  ";

        if ($busca) {  
        } 

        $query .= " GROUP BY M.cdparceiromotorista ";
//        echo $query; 
        $result = $conection->query($query);
        while ($rows = $conection->fetch_array($result)) {
            $return[] = $rows;
        }
        return $return;
    }
    
}
