<?php

include_once $_SERVER['DOCUMENT_ROOT']. '/models/connection.php';

class usuario extends connection {

    function insertUsuario($cdusuariotipo, $txusuario, $txnome, $txemail, $txrg, $txcpf, $txtelefone, $txcelular, $txsenha) {

        $conection = new connection();
        $query = "INSERT INTO usuario "
                . "(cdusuariotipo, txusuario, txnome, txemail, txrg, txcpf, txtelefone, txcelular, txsenha, "
                . " flsituacao, flexcluido) "
                . " VALUES "
                . "('$cdusuariotipo', '$txusuario', '$txnome', '$txemail', '$txrg', '$txcpf', '$txtelefone', '$txcelular', "
                . " '$txsenha', 0, 0)";
//        echo $query;
        $result = $conection->query($query);
        if ($result) {
            $id = $conection->insert_id();
        } else {
            $id = 0;
        }
        return $id;
    }

    function updateUsuario($cdusuario, $cdusuariotipo, $txusuario, $txnome, $txemail, $txrg, $txcpf, $txtelefone, $txcelular) {
        $conection = new connection();
        $query = "UPDATE usuario SET "
                . "cdusuariotipo = '$cdusuariotipo', txusuario = '$txusuario', txnome = '$txnome', txemail = '$txemail', "
                . "txrg = '$txrg', txcpf = '$txcpf', txtelefone = '$txtelefone', txcelular = '$txcelular' "
                . "WHERE cdusuario = $cdusuario ";
// echo $query;
        $result = $conection->query($query);

        return $result;
    }

    function updateSituacaoUsuario($cdusuario, $flsituacao) {
        $conection = new connection();
        $query = "UPDATE usuario SET "
                . "flsituacao = $flsituacao "
                . "WHERE cdusuario = $cdusuario ";
//echo $query;

        $result = $conection->query($query);

        return $result;
    }

    function updateSenhaUsuario($cdusuario, $txsenha) {

        $txsenha = md5($txsenha);

        $conection = new connection();
        $query = "UPDATE usuario SET "
                . "txsenha = '$txsenha' "
                . "WHERE cdusuario = $cdusuario ";
//echo $query;

        $result = $conection->query($query);

        return $result;
    }

    function deleteUsuario($cdusuario) {
        $conection = new connection();
        $query = "UPDATE usuario SET "
                . "flexcluido = 1 "
                . "WHERE cdusuario = $cdusuario ";

        $result = $conection->query($query);

        return $result;
    }

    function retornaUsuario($cdusuario) {
        $conection = new connection();
        $query = "SELECT * FROM usuario "
                . "WHERE cdusuario = $cdusuario AND flexcluido <> 1 ";

        $result = $conection->query($query);
        $rows = $conection->fetch_array($result);
        return $rows;
    }

    function retornaUsuarioEmail($txemail) {
        $conection = new connection();
        $query = "SELECT * FROM usuario "
                . "WHERE txemail = '$txemail' AND flexcluido <> 1 ";

        $result = $conection->query($query);
        $rows = $conection->fetch_array($result);
        return $rows;
    }

    function retornaUsuarioLogin($txemail, $txsenha) {
        $txsenha = MD5($txsenha);
        $conection = new connection();
        $query = "SELECT * FROM usuario "
                . "WHERE txemail = '$txemail' AND txsenha = '$txsenha' "
                . "AND flexcluido <> 1 AND flsituacao =1 ";
//echo "$query";
        $result = $conection->query($query);
        $rows = $conection->fetch_array($result);
        return $rows;
    }

    function retornaqtdeUsuario($filtro = null) {
        $conection = new connection();
        $query = "SELECT count(DISTINCT cdusuario) as total FROM usuario "
                . "WHERE flexcluido <> 1 ";

        if ($filtro['flsituacao']) {
            $query .= " AND flsituacao = $filtro[flsituacao] ";
        }

        $result = $conection->query($query);
        $rows = $conection->fetch_array($result);
        return $rows['total'];
    }

    function listaUsuario($pg = null, $qtde = null, $filtro = null) {
        $conection = new connection();
        $query = "SELECT * FROM usuario "
                . "WHERE flexcluido <> 1 AND cdusuariotipo > 1 ";

        if ($filtro['flsituacao']) {
            $query .= " AND flsituacao = $filtro[flsituacao] ";
        }

        $result = $conection->query($query);

        for ($i = 0; $i < $result->num_rows; $i++) {
            $rows[] = $conection->fetch_array($result);
        }

        return $rows;
    }

    /*     * ************Usuario Logado************** */

    function insertUsuarioLogado($cdusuario, $codigo) {
        $conection = new connection();
        $query = "INSERT INTO usuariologado "
                . "(cdusuario, codigo) "
                . " VALUES "
                . "('$cdusuario', '$codigo')";

        $result = $conection->query($query);
        if ($result) {
            $id = $conection->insert_id();
        } else {
            $id = 0;
        }
        return $id;
    }

    function updateUsuarioLogado($cdlogado, $cdusuario, $codigo) {
        $conection = new connection();
        $query = " UPDATE usuariologado SET "
                . " cdusuario = '$cdusuario', codigo = '$codigo' "
                . " WHERE cdlogado = $cdlogado ";

        $result = $conection->query($query);

        return $result;
    }

    function deleteUsuarioLogado($cdusuario) {
        $conection = new connection();

        $query = "DELETE FROM usuariologado "
                . "WHERE cdusuario = $cdusuario ";

        $result = $conection->query($query);
        $rows = $conection->fetch_array($result);
        return $rows;
    }

    function retornaUsuarioLogado($cdusuario) {
        $conection = new connection();
        $query = "SELECT * FROM usuariologado "
                . "WHERE cdusuario = $cdusuario ";
        $result = $conection->query($query);
        $rows = $conection->fetch_array($result);
        return $rows;
    }

    function retornaUsuarioLogadoCodigo($codigo) {
        $conection = new connection();
        $query = "SELECT L.*, U.* FROM usuariologado L, usuario U "
                . "WHERE L.cdusuario = U.cdusuario AND L.codigo = '$codigo' ";

        $result = $conection->query($query);
        $rows = $conection->fetch_array($result);
        return $rows;
    }

}
