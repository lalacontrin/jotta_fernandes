<?php

include_once $_SERVER['DOCUMENT_ROOT']. '/models/connection.php';

class parceiro extends connection {

    function insertParceiro($txparceiro, $txnome_nomefantasia, $txrazao, $txtipo, $txcpf_cnpj, $txrg, $txendereco, $txnumero, $txcomplemento, $txbairro, $txcidade, $txestado, $txresponsavel, $flpainel, $flsituacao, $txtelefone, $txcelular) {

        $conection = new connection();

        $hj = date("Y-m-d H:i:s");

        if ($flsituacao) {
            $dtaprovado = $hj;
        } else {
            $dtaprovado = "";
        }

        $query = "INSERT INTO parceiro "
                . "(txparceiro, txnome_nomefantasia, txrazao, txtipo, txcpf_cnpj, txrg, txendereco, txnumero, txcomplemento, txbairro, txcidade, txestado, txresponsavel, dtcadastro, dtaprovado, flpainel, flativo, flsituacao, flexcluido, txtelefone, txcelular) "
                . " VALUES "
                . "('$txparceiro', '$txnome_nomefantasia', '$txrazao', '$txtipo', '$txcpf_cnpj', '$txrg', '$txendereco', '$txnumero', '$txcomplemento', '$txbairro', '$txcidade', '$txestado', '$txresponsavel', '$hj', '$dtaprovado', '$flpainel', 1, '$flsituacao', 0, '$txtelefone', '$txcelular')";
 //echo $query;
        $result = $conection->query($query);
        if ($result) {
            $id = $conection->insert_id();
        } else {
            $id = 0;
        }
        return $id;
    }

    function updateParceiro($cdparceiro, $txparceiro, $txnome_nomefantasia, $txrazao, $txtipo, $txcpf_cnpj, $txrg, $txendereco, $txnumero, $txcomplemento, $txbairro, $txcidade, $txestado, $txresponsavel, $flsituacao, $txtelefone, $txcelular) {

        $conection = new connection();
        $query = " UPDATE parceiro SET "
                . " txparceiro = '$txparceiro', txnome_nomefantasia = '$txnome_nomefantasia', txrazao = '$txrazao',"
                . " txtipo = '$txtipo', txcpf_cnpj = '$txcpf_cnpj', txrg = '$txrg', txendereco = '$txendereco',  "
                . " txnumero = '$txnumero', txcomplemento = '$txcomplemento', txbairro = '$txbairro',  "
                . " txcidade = '$txcidade', txestado = '$txestado', txresponsavel = '$txresponsavel',  "
                . " txtelefone = '$txtelefone', txcelular = '$txcelular', "
                . " flsituacao = '$flsituacao'   "
                . "WHERE cdparceiro = $cdparceiro ";

 echo $query;
        $result = $conection->query($query);

        return $result;
    }

    function updateSituacaoParceiro($cdparceiro, $flsituacao) {
        $conection = new connection();
        $query = "UPDATE parceiro SET "
                . "flsituacao = $flsituacao "
                . "WHERE cdparceiro = $cdparceiro ";
//echo $query;
        $result = $conection->query($query);

        return $result;
    }

    function deleteParceiro($cdparceiro) {
        $conection = new connection();
        $query = "UPDATE parceiro SET "
                . "flexcluido = 1 "
                . "WHERE cdparceiro = $cdparceiro ";

        $result = $conection->query($query);

        return $result;
    }

    function retornaParceiro($cdparceiro) {
        $conection = new connection();
        $query = "SELECT F.*  FROM parceiro F  "
                . "WHERE  F.cdparceiro = $cdparceiro AND F.flexcluido <> 1 ";

        $result = $conection->query($query);
        $rows = $conection->fetch_array($result);
        return $rows;
    }

    function retornaQuantidadesParceiro($busca = null) {
        $conection = new connection();
        $query = "  SELECT count(DISTINCT M.cdparceiro) as total "
                . " FROM parceiro M  "
                . " WHERE  M.flexcluido <> 1  ";

        if ($busca) {
            
        }

//        echo $query;
        $result = $conection->query($query);
        $rows = $conection->fetch_array($result);
        return $rows;
    }

    function retornaTodosParceiro($busca, $tipo) {
        $conection = new connection();
        $query = "  SELECT M.* "
                . " FROM parceiro M "
                . " WHERE M.flexcluido <> 1  ";

        if ($busca) {
            
        }

        $query .= " GROUP BY M.cdparceiro ";
//        echo $query; 
        $result = $conection->query($query);
        while ($rows = $conection->fetch_array($result)) {
            $return[] = $rows;
        }
        return $return;
    }

    /*     * **********************parceiro dia*********************** */

    function insertParceiroDia($cdparceiro, $cddia) {

        $conection = new connection();

        $query = "INSERT INTO parceirodia "
                . "(cdparceiro, cddia) "
                . " VALUES "
                . "('$cdparceiro', '$cddia')";
// echo $query;
        $result = $conection->query($query);
        if ($result) {
            $id = $conection->insert_id();
        } else {
            $id = 0;
        }
        return $id;
    }

    function deleteTodosParceiroDia($cdparceiro) {
        $conection = new connection();
        $query = "DELETE FROM parceirodia "
                . "WHERE cdparceiro = $cdparceiro ";

        $result = $conection->query($query);

        return $result;
    }

    function retornaParceiroDia($cdparceiro, $cddia) {
        $conection = new connection();
        $query = "SELECT F.*  FROM parceirodia F  "
                . "WHERE  F.cdparceiro = $cdparceiro AND F.cddia = $cddia ";

        $result = $conection->query($query);
        $rows = $conection->fetch_array($result);
        return $rows;
    }

    /*     * **********************parceiro Cidade Estado*********************** */

    function insertParceiroEstadoCidade($cdparceiro, $cdestado, $cdestadocidade) {

        $conection = new connection();

         $query = "INSERT INTO parceiroestadocidade "
                . "(cdparceiro, cdestado, cdestadocidade) "
                . " VALUES "
                . "('$cdparceiro', '$cdestado', '$cdestadocidade')";

// echo $query;
        $result = $conection->query($query);
        if ($result) {
            $id = $conection->insert_id();
        } else {
            $id = 0;
        }
        return $id;
    }

    function deleteTodosParceiroEstadoCidade($cdparceiro) {
        $conection = new connection(); 
        $query = "DELETE FROM parceiroestadocidade "
                . "WHERE cdparceiro = $cdparceiro ";

        $result = $conection->query($query);

        return $result;
    }

    function retornaParceiroEstadoCidade($cdparceiro, $cdestado, $cdestadocidade) {
        $conection = new connection();
        $query = "SELECT F.*  FROM parceiroestadocidade F  "
                . "WHERE  F.cdparceiro = $cdparceiro AND F.cdestado = $cdestado  "
                . "AND F.cdestadocidade = $cdestadocidade ";

        $result = $conection->query($query);
        $rows = $conection->fetch_array($result);
        return $rows;
    }

}
