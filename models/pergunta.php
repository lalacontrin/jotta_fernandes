<?php

include_once $_SERVER['DOCUMENT_ROOT']. '/models/connection.php';

class pergunta extends connection {

    function insertPergunta($txpergunta, $fltipo, $flsituacao) {

        $conection = new connection(); 

        $query = "INSERT INTO pergunta "
                . "(txpergunta, fltipo, flsituacao, flexcluido) "
                . " VALUES "
                . "('$txpergunta', '$fltipo', '$flsituacao', 0)";
// echo $query;
        $result = $conection->query($query);
        if ($result) {
            $id = $conection->insert_id();
        } else {
            $id = 0;
        }
        return $id;
    }

    function updatePergunta($cdpergunta, $txpergunta, $fltipo, $flsituacao) {

        $conection = new connection();
        $query = " UPDATE pergunta SET "
                . " txpergunta = '$txpergunta', fltipo = '$fltipo', "
                . " flsituacao = '$flsituacao'   "
                . "WHERE cdpergunta = $cdpergunta ";

// echo $query;
        $result = $conection->query($query);

        return $result;
    }

    function updateSituacaoPergunta($cdpergunta, $flsituacao) {
        $conection = new connection();
        $query = "UPDATE pergunta SET "
                . "flsituacao = $flsituacao "
                . "WHERE cdpergunta = $cdpergunta ";
//echo $query;
        $result = $conection->query($query);

        return $result;
    }

    function deletePergunta($cdpergunta) {
        $conection = new connection();
        $query = "UPDATE pergunta SET "
                . "flexcluido = 1 "
                . "WHERE cdpergunta = $cdpergunta ";

        $result = $conection->query($query);

        return $result;
    }

    function retornaPergunta($cdpergunta) {
        $conection = new connection();
        $query = "SELECT F.*  FROM pergunta F  "
                . "WHERE  F.cdpergunta = $cdpergunta AND F.flexcluido <> 1 ";

        $result = $conection->query($query);
        $rows = $conection->fetch_array($result);
        return $rows;
    }

    function retornaQuantidadesPergunta($busca = null) {
        $conection = new connection();
        $query = "  SELECT count(DISTINCT M.cdpergunta) as total "
                . " FROM pergunta M  "
                . " WHERE  M.flexcluido <> 1  ";

        if ($busca) {
            
        }

//        echo $query;
        $result = $conection->query($query);
        $rows = $conection->fetch_array($result);
        return $rows;
    }

    function retornaTodosPergunta($busca, $tipo) {
        $conection = new connection();
        $query = "  SELECT M.* "
                . " FROM pergunta M "
                . " WHERE M.flexcluido <> 1  ";

        if ($busca) {
            
        }

        $query .= " GROUP BY M.cdpergunta ";
//        echo $query; 
        $result = $conection->query($query);
        while ($rows = $conection->fetch_array($result)) {
            $return[] = $rows;
        }
        return $return;
    }
 
    
    function insertPerguntaRespostas($cdpergunta, $txperguntarespostas, $fltipo, $flsituacao) {

        $conection = new connection(); 

        $query = "INSERT INTO perguntarespostas "
                . "(cdpergunta, txperguntarespostas, fltipo, flsituacao, flexcluido) "
                . " VALUES "
                . "('$cdpergunta', '$txperguntarespostas', '$fltipo', '$flsituacao', 0)";
// echo $query;
        $result = $conection->query($query);
        if ($result) {
            $id = $conection->insert_id();
        } else {
            $id = 0;
        }
        return $id;
    }

    function updatePerguntaRespostas($cdperguntarespostas, $cdpergunta, $txperguntarespostas, $fltipo, $flsituacao) {

        $conection = new connection();
        $query = " UPDATE perguntarespostas SET "
                . " cdpergunta = $cdpergunta, txperguntarespostas = '$txperguntarespostas', "
                . " fltipo = '$fltipo', flsituacao = '$flsituacao'   "
                . "WHERE cdperguntarespostas = $cdperguntarespostas ";

// echo $query;
        $result = $conection->query($query);

        return $result;
    }

    function deletePerguntaRespostas($cdperguntarespostas) {
        $conection = new connection();
        $query = "UPDATE perguntarespostas SET "
                . "flexcluido = 1 "
                . "WHERE cdperguntarespostas = $cdperguntarespostas ";

        $result = $conection->query($query);

        return $result;
    }

    function retornaPerguntaRespostas($cdperguntarespostas) {
        $conection = new connection();
        $query = "SELECT F.*  FROM perguntarespostas F  "
                . "WHERE  F.cdperguntarespostas = $cdperguntarespostas AND F.flexcluido <> 1 ";

        $result = $conection->query($query);
        $rows = $conection->fetch_array($result);
        return $rows;
    }

    function retornaTodosPerguntaRespostas($cdpergunta) {
        $conection = new connection();
        $query = "  SELECT M.* "
                . " FROM perguntarespostas M "
                . " WHERE M.flexcluido <> 1  ";

        $query .= " GROUP BY M.cdperguntarespostas ";
//        echo $query; 
        $result = $conection->query($query);
        while ($rows = $conection->fetch_array($result)) {
            $return[] = $rows;
        }
        return $return;
    }
}
