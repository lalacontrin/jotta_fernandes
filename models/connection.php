<?php

include_once $_SERVER['DOCUMENT_ROOT']. '/includes/auxiliares.php';
//header('Content-Type: text/html; charset=utf-8');

class connection {

    var $host = "mysql995.umbler.com";
    var $port = "";
    var $user = "jottaf";
    var $password = "j0tta134";
    var $db = "jottaf";
    var $con;

    function connection() {
        $this->con = mysqli_connect("$this->host", $this->user, $this->password, $this->db);
        if (!$this->con) {
            echo "Connection error";
            return false;
        }

        return $this->con;
    }

    function query($query) {
        $result = mysqli_query($this->con, $query);
        return $result;
    }

    function fetch_array($result, $tipo = null) {
        if (!$tipo) {
            $tipo = MYSQLI_ASSOC;
        }
       // return mysqli_fetch_array($result, $tipo);
        return utf8_encode_array(mysqli_fetch_array($result, $tipo));
    }

    function insert_id() {
        $id = mysqli_insert_id($this->con);
        return $id;
    }

    function close() {
        mysqli_close($this->con);
        return true;
    }

}
