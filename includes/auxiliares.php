<?php

function utf8_encode_array($input) {
    if (is_array($input)) {
        $aRes = array();
        foreach ($input as $indice => $valor) {
            if (is_array($valor)) {
                $aRes[$indice] = utf8_encode_array($valor);
            } else {
                $aRes[$indice] = utf8_encode($valor);
            }
        }
        return $aRes;
    }
}

function utf8_decode_array($input) {
    if (is_array($input)) {
        $aRes = array();
        foreach ($input as $indice => $valor) {
            if (is_array($valor)) {
                $aRes[$indice] = utf8_decode_array($valor);
            } else {
                $aRes[$indice] = utf8_decode($valor);
            }
        }
        return $aRes;
    }
}

function retornaMes($mes) {
    switch ($mes) {
        case 1:
        case '1':
            return "Janeiro";
            break;
        case 2:
        case '2':
            return "Fevereiro";
            break;
        case 3:
        case '3':
            return "Mar�o";
            break;
        case 4:
        case '4':
            return "Abril";
            break;
        case 5:
        case '5':
            return "Maio";
            break;
        case 6:
        case '6':
            return "Junho";
            break;
        case 7:
        case '7':
            return "Julho";
            break;
        case 8:
        case '8':
            return "Agosto";
            break;
        case 9:
        case '9':
            return "Setembro";
            break;
        case 10:
        case '10':
            return "Outubro";
            break;
        case 11:
        case '11':
            return "Novembro";
            break;
        case 12:
        case '12':
            return "Dezembro";
            break;
    }
}

function enviaEmail($txemail, $subject, $conteudo) {
    /* Enviando a mensagem */

    include_once("models/class.phpmailer.php");


    // Inicia a classe PHPMailer
    $mail = new PHPMailer(); 
    $mail->IsSMTP(); // Define que a mensagem ser� SMTP
    $mail->SMTPDebug = 1;
    $mail->Port = 587; //Indica a porta de conex�o 
    $mail->Host = "smtp.umbler.com"; // Endereo do servidor SMTP
    $mail->SMTPAuth = true; // Usa autenticacaoo SMTP? (opcional)

    $mail->Username = 'no-reply@jottafernandes.com.br'; // Usurio do servidor SMTP
    $mail->Password = 'j0tta134'; // Senha do servidor SMTP
    $mail->From = "no-reply@jottafernandes.com.br"; // Seu e-mail
    $mail->FromName = "Jotta Fernandes"; // Seu nome
    $mail->IsHTML(true); // Define que o e-mail ser� enviado como HTML  
    $mail->Subject =  $subject;
    $mail->Body = $conteudo;


    $mail->AddAddress($txemail, $txemail);


    // Envia o e-mail
    $result = $mail->Send();

    // Limpa os destinatarios e os anexos
    $mail->ClearAllRecipients();
    $mail->ClearAttachments();
}
