
<aside class="side-bar _d-flex _flex-column">
    <div class="menuBox _fg-1">

        <div class="menuGroup">
            <a href="/painel/modelos-de-veiculo" class="side-bar-link typo-body-2 typo-ff typo-fw-medium">
                <i class="svg-icon -md">
                    <svg><use xlink:href="#icon-cli" /></svg>
                </i>
                Modelos de Veiculo
            </a>
        </div>

        <div class="menuGroup">
            <a href="/painel/tipos-de-veiculo" class="side-bar-link typo-body-2 typo-ff typo-fw-medium">
                <i class="svg-icon -md">
                    <svg><use xlink:href="#icon-cli" /></svg>
                </i>
                Tipos de Veiculo
            </a>
        </div>

        <div class="menuGroup">
            <a href="/painel/parceiros" class="side-bar-link typo-body-2 typo-ff typo-fw-medium">
                <i class="svg-icon -md">
                    <svg><use xlink:href="#icon-cli" /></svg>
                </i>
                Parceiros
            </a>
        </div>

        <div class="menuGroup">
            <a href="/painel/perguntas" class="side-bar-link typo-body-2 typo-ff typo-fw-medium">
                <i class="svg-icon -md">
                    <svg><use xlink:href="#icon-cli" /></svg>
                </i>
                Perguntas
            </a>
        </div>

        <div class="menuGroup">
            <a href="/painel/orcamentos" class="side-bar-link typo-body-2 typo-ff typo-fw-medium">
                <i class="svg-icon -md">
                    <svg><use xlink:href="#icon-file" /></svg>
                </i>
                Orçamentos
            </a>
            <div class="boxSub">
                <a href="/painel/orcamentos/1" class="side-bar-link typo-body-2 typo-ff typo-fw-medium">
                    Fechados
                </a>
                <a href="/painel/orcamentos/2" class="side-bar-link typo-body-2 typo-ff typo-fw-medium">
                    Abertos
                </a> 
            </div>
        </div>

        <div class="menuGroup">
            <a href="/painel/usuarios" class="side-bar-link typo-body-2 typo-ff typo-fw-medium">
                <i class="svg-icon -md">
                    <svg><use xlink:href="#icon-account-group" /></svg>
                </i>
                Usuários
            </a>
        </div>

    </div>
    <div class="_fs-0">
        <a href="/painel/logout" class="side-bar-link typo-body-2 typo-ff typo-fw-medium">
            <i class="svg-icon -md">
                <svg><use xlink:href="#icon-power" /></svg>
            </i>
            Sair
        </a>
    </div>
</aside>