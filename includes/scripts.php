<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/main.min.js"></script>
<script src="/assets/js/jquery.mask.min.js"></script>
<script>
    function abrirAvisos(tipo, texto, classPai) {
        if (!classPai) {
            classPai = "";
        }

        $(classPai + "#spanAviso").html(texto).removeClass('-error').removeClass('-success').removeClass('-info').removeClass('-warning').addClass(tipo);
        $(classPai + "#cxAviso").removeClass('hide');
        $('html,body').animate({scrollTop: $(classPai + "#cxAviso").offset().top - 120}, 1000);
    }

    function fecharAvisos() {
        $(".cxAviso").addClass('hide')
    }
 
</script>

