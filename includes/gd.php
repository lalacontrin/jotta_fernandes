<?php

// mais uma vez, mantendo a sess�o aberta
session_start();

// fun��o do gd, para criar uma imagem, com o tamanho definido
// largura, altura
$imagem = imagecreate(35, 15);

// fun��o que define a cor de fundo da imagem gerada pelo gd
// em meu caso, amarelo
$fundo = imagecolorallocate($imagem, 204, 204, 204);

// fun��o que define a cor da fonte, em meu caso, preto.
$fonte = imagecolorallocate($imagem, 255, 255, 255);

// desenhando a imagem, baseada nos padr�es informados acima
// verificando a sess�o aberta, para informar ao formul�rio o que foi digitado
imagestring($imagem, 4, 0, 0, $_SESSION['codigo'], $fonte);

// header, necess�rio
header("Content-type: image/png");

// formato da imagem, no meu caso utilizei PNG.
// vc pode usar imagejpeg, imagegif, etc. Veja as refer�ncias no manual do php
imagepng($imagem);
