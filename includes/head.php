<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="shortcut icon" href="email/img/favicon-16x16.png" />

<!-- SEO Geral -->
<title>Jotta Fernandes</title>
<meta name="description" content="Jotta Fernandes">
<meta name="author" content="O2 info">
<meta name="robots" content="index">

<!-- Google+ / Schema.org -->
<meta itemprop="name" content="Jotta Fernandes">
<meta itemprop="description" content="Jotta Fernandes">
<meta itemprop="image" content="/assets/img/logo-client.png">

<!-- Open Graph Facebook -->
<meta property="og:title" content="Jotta Fernandes">
<meta property="og:description" content="Jotta Fernandes" />
<meta property="og:site_name" content="Jotta Fernandes" />
<meta property="og:type" content="website">
<meta property="og:image" content="/assets/img/logo-client.png">

<!-- Twitter -->
<meta name="twitter:title" content="Jotta Fernandes">
<meta name="twitter:description" content="Jotta Fernandes">
<meta name="twitter:image" content="/assets/img/logo-client.png">