<script src="/assets/js/jquery.min.js"></script> 
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="/assets/js/app.bundle.js" async></script>
<script src="/assets/js/scripts.js"></script>
<script src="/assets/js/jquery.mask.min.js"></script>
<script>
    function abrirAvisos(tipo, texto, classPai) {
        if (!classPai) {
            classPai = "";
        }

        $(classPai + "#divAviso").removeClass('-error').removeClass('-success').addClass(tipo);
        $(classPai + "#iconAviso").html(`<svg><use xlink:href="#icon-alert${tipo}" /></svg>`);
        $(classPai + "#divAviso").html(texto);
        $(classPai + "#cxAviso").removeClass('-hide');
        $('html,body').animate({scrollTop: $(classPai + "#cxAviso").offset().top - 120}, 1000);
    }

    function fecharAvisos() {
        $(".cxAviso").addClass('-hide')
    }

</script>

