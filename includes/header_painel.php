
<header class="header-bar">
    <div class="first _d-flex _al-center">
        <a class="burguerMenu thumbIcon svg-icon -white _inline-flex _al-center _jc-center _mr-md js-menu-open">
            <i class="svg-icon -md -white">
                <svg><use xlink:href="#icon-menu" /></svg>
            </i>
        </a>
        <span class="textLogo typo-color-dark-text-primary typo-headline _upper">logo</span>
    </div>
    <div class="second _d-flex _al-center">
        <div class="_fg-1 _d-flex _al-center">
            <div class="thumbIcon svg-icon -white _inline-flex _al-center _jc-center _mr-sm">
                <i class="svg-icon -md -white">
                    <svg><use xlink:href="#icon-user" /></svg>
                </i>
            </div>
            <span class="textProfile typo-color-light-text-primary typo-sub-heading">Olá, <?= $_SESSION['txnome'] ?></span>
        </div>
<!--        <div class="_fs-0">
             class="-notification" quando houver notificacoes 
            <a href="#" class="thumbIcon -notification svg-icon -white _inline-flex _al-center _jc-center">
                <i class="svg-icon -md -white">
                    <svg><use xlink:href="#icon-notification" /></svg>
                </i>
            </a>
        </div>-->
    </div>
</header>