<?php

if ($_SESSION['cdusuario']) {
    if ($tipoPage == "logar") {
        echo '<script>location.href="/painel/parceiros"</script>';
        exit;
    }
} else if ($_COOKIE['codigo']) {
    $dados = retornaUsuarioLogadoCodigo($_COOKIE['codigo']);
    if ($dados) {
        $_SESSION['cdusuario'] = $dados['cdusuario'];
        $_SESSION['txnome'] = $dados['txnome'];
        if (!retornaUsuarioLogado($dados['cdusuario'])) {
            insertUsuarioLogado($dados['cdusuario'], md5($dados['cdusuario']));
        }

        setcookie("codigo", md5($dados['cdusuario']), time() + (3600 * 360));

    }
}

if (!$_SESSION['cdusuario']) {
    if ($tipoPage != "logar") {
        echo '<script>location.href="/painel"</script>';
        exit;
    }
}

