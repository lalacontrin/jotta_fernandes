<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/controllers.php';
$tipoPage = 'parceiros';
include_once $_SERVER['DOCUMENT_ROOT']. '/includes/session_painel.php';
if ($_GET['codigo']) {
    $dadoParceiro = retornaParceiro($_GET['codigo']);
}
if ($_GET['codigo2']) {
    $dado = retornaParceiroVeiculo($_GET['codigo2']);
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <?php
        include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/head_painel.php';
        include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/stylesheets_painel.php';
        include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/scripts_painel.php';
        ?> 
        <script>
            function Gravar() {
                $("#btSalvar").removeAttr('href');
                $(".text-field").removeClass('-error');
                form = document.dados;
                flerro = 0;
                fecharAvisos();
                if (form.cdparceiroveiculomodelo.value == "") {
                    flerro = 1;
                    $("#cdparceiroveiculomodelo").addClass('-error')
                }
                if (form.cdparceiroveiculotipo.value == "") {
                    flerro = 1;
                    $("#cdparceiroveiculotipo").addClass('-error')
                }
                if (form.txplaca.value == "") {
                    flerro = 1;
                    $("#txplaca").addClass('-error')
                }
                if (form.txrenavam.value == "") {
                    flerro = 1;
                    $("#txrenavam").addClass('-error')
                }
                if (form.txmarca_modelo.value == "") {
                    flerro = 1;
                    $("#txmarca_modelo").addClass('-error')
                }
                if (form.txanofabricacao.value == "") {
                    flerro = 1;
                    $("#txanofabricacao").addClass('-error')
                }
                if (form.txanomodelo.value == "") {
                    flerro = 1;
                    $("#txanomodelo").addClass('-error')
                }
                if (form.txcor.value == "") {
                    flerro = 1;
                    $("#txcor").addClass('-error')
                }
                if (form.vlcapacidade.value == "") {
                    flerro = 1;
                    $("#vlcapacidade").addClass('-error')
                }
                if (flerro) {
                    abrirAvisos('-error', 'Preencha todos os campos marcados.');
                    $("#btSalvar").attr('href', 'javascript:Gravar()');
                    return false;
                } else {
                    f_data = new FormData(form);
                    $.ajax({
                        type: "POST",
                        url: "/painel/acao.php",
                        data: f_data,
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            abrirAvisos('-success', "Dados gravados com sucesso", "");
                            setTimeout(function () {
                                location.href = "/painel/novo-parceiro/<?= $_GET['codigo'] ?>";
                            }, 2000);
                        }
                    })
                }
            }

        </script>
    </head>
    <body>
        <div class="side-bar-overlay js-menu-close"></div>
        <main class="main-box">
            <?php
            include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/header_painel.php';
            include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/menu_painel.php';
            ?>
            <div class="content-main">
                <div class="inner">
                    <div class="title container-block">
                        <h4 class="typo-color-light-text-primary typo-display-1 _d-block">Veiculos</h4>
                        <div class="breadcrumbs-container _mt-sm">
                            <a href="/painel/parceiros" class="link typo-body-2 typo-color-light-text-primary">Parceiros</a>
                            <a class="link typo-body-2 typo-color-light-text-primary"><?= $dadoParceiro['txparceiro'] ?> / Veiculo</a>
                        </div>
                    </div>
                    <div class="grid-row -gutter-xl">
                        <div class="col-12">
                            <div class="card-box"> 
                                <div class="body">
                                    <form name="dados" id="dados" method="POST" action="javascript:Gravar()" class="grid-row -gutter-xl">
                                        <input type="hidden" id="acao" name="acao" value="gravarVeiculo"/>
                                        <input type="hidden" id="cdparceiro" name="cdparceiro" value="<?= $_GET['codigo'] ?>"/>
                                        <input type="hidden" id="cdveiculo" name="cdveiculo" value="<?= $dado['cdparceiroveiculo'] ?>"/>
                                        <div id="cxAviso" class="col-12 -hide cxAviso">
                                            <div id="divAviso" class="alert-box -success _d-flex _al-center">
                                                <i class="svg-icon -md -white _mr-sm" id="iconAviso">
                                                    <svg><use xlink:href="#icon-alert-success" /></svg>
                                                </i>
                                                <span class="text" id="spanAviso">Sua mensagem enviada com sucesso!</span>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper">Modelo</label>
                                            <select id="cdparceiroveiculomodelo" name="cdparceiroveiculomodelo" class="text-field">
                                                <option>Escolha o modelo do veiculo</option> 
                                                <?php
                                                $modelos = retornaTodosParceiroveiculomodelo();
                                                if ($modelos) {
                                                    foreach ($modelos as $t) {
                                                        $sel = "";
                                                        if ($t['cdparceiroveiculomodelo'] == $dado['cdparceiroveiculomodelo']) {
                                                            $sel = "selected";
                                                        }
                                                        ?>
                                                        <option <?= $sel ?> value="<?= $t['cdparceiroveiculomodelo'] ?>"><?= $t['txparceiroveiculomodelo'] ?></option>  
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>

                                        <div class="col-md-4">
                                            <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper">Tipo</label>
                                            <select id="cdparceiroveiculotipo" name="cdparceiroveiculotipo" class="text-field">
                                                <option>Escolha o tipo do veiculo</option> 
                                                <?php
                                                $tipos = retornaTodosParceiroveiculotipo();
                                                if ($tipos) {
                                                    foreach ($tipos as $t) {
                                                        $sel = "";
                                                        if ($t['cdparceiroveiculotipo'] == $dado['cdparceiroveiculotipo']) {
                                                            $sel = "selected";
                                                        }
                                                        ?>
                                                        <option <?= $sel ?> value="<?= $t['cdparceiroveiculotipo'] ?>"><?= $t['txparceiroveiculotipo'] ?></option>  
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>

                                        <div class="col-md-4">
                                            <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper">Placa <span class="typo-color-error">*</span></label>
                                            <input type="text" name="txplaca" value="<?= $dado['txplaca'] ?>" id="txplaca" class="text-field ">
                                        </div>

                                        <div class="col-md-4">
                                            <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper">Renavam<span class="typo-color-error">*</span></label>
                                            <input type="text" name="txrenavam" value="<?= $dado['txrenavam'] ?>"   id="txrenavam"  class="text-field ">
                                        </div>

                                        <div class="col-md-4 camposPJ">
                                            <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper" >Marca / Modelo<span class="typo-color-error">*</span></label>
                                            <input type="text" name="txmarca_modelo"  id="txmarca_modelo" value="<?= $dado['txmarca_modelo'] ?>"   class="text-field ">
                                        </div>

                                        <div class="col-md-4">
                                            <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper" >Ano Fabricação<span class="typo-color-error">*</span></label>
                                            <input type="tel" name="txanofabricacao"  id="txanofabricacao"  value="<?= $dado['txanofabricacao'] ?>"  class="text-field ">
                                        </div>

                                        <div class="col-md-4">
                                            <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper" >Ano Modelo<span class="typo-color-error">*</span></label>
                                            <input type="tel" name="txanomodelo"  id="txanomodelo"  value="<?= $dado['txanomodelo'] ?>" class="text-field ">
                                        </div>

                                        <div class="col-md-4">
                                            <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper" >Cor<span class="typo-color-error">*</span></label>
                                            <input type="text" name="txcor"  id="txcor"  value="<?= $dado['txcor'] ?>" class="text-field ">
                                        </div>

                                        <div class="col-md-4">
                                            <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper" >Capacidade<span class="typo-color-error">*</span></label>
                                            <input type="text" name="vlcapacidade"  id="vlcapacidade"  value="<?= $dado['vlcapacidade'] ?>" class="text-field ">
                                        </div>
                                    </form>
                                </div>
                                <div class="footer _d-flex">
                                    <div class="_fg-1">
                                        <a href="javascript:window.history.back();" class="button-base -outline">Voltar</a>
                                    </div>
                                    <div class="_fs-0">
                                        <a href="javascript:Gravar()" id="btSalvar" class="button-base">Salvar</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </main>
    </body>
</html>
