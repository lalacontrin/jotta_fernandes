<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/controllers.php';
$tipoPage = 'parceiros';
include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/session_painel.php';
if ($_GET['codigo']) {
    $dado = retornaParceiro($_GET['codigo']);
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <?php
        include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/head_painel.php';
        include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/stylesheets_painel.php';
        include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/scripts_painel.php';
        ?>
        <script src="/assets/js/cidades-estados-1.2-utf8.js"></script>
        <script>

            $(document).ready(function () {

<?php if ($_GET['codigo']) { ?>
                    var txestado = '<?= $dado['txestado'] ?>';
                    var txcidade = '<?= $dado['txcidade'] ?>';
                    new dgCidadesEstados({
                        estado: $('#txestado').get(0),
                        cidade: $('#txcidade').get(0),
                        estadoVal: txestado,
                        cidadeVal: txcidade
                    });
    <?php if ($dado['txtipo'] == 'pj') {
        ?>
                        selecionaTipoPessoa(1);
    <?php } else { ?>
                        selecionaTipoPessoa(2);
        <?php
    }
} else {
    ?>

                    new dgCidadesEstados({
                        estado: $('#txestado').get(0),
                        cidade: $('#txcidade').get(0)
                    });
<?php } ?>

<?php if ($_GET['retorno'] == 'sucesso') { ?>
                    abrirAvisos('-success', "Adicionado com sucesso", "");
<?php } ?>
            })

            function excluirVeiculo(cdveiculo) {
                Swal.fire({
                    title: 'Deseja remover este registro?',
                    text: "Você não poderá reverter isso!",
                    type: 'warning',
                    showCancelButton: true,
                    buttonsStyling: false,
                    confirmButtonText: 'Sim, remover!',
                    cancelButtonText: "Cancelar",
                    customClass: {
                        confirmButton: 'button-base',
                        cancelButton: 'button-base -cancel'
                    },
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: "POST",
                            url: "/painel/acao.php",
                            data: {acao: 'excluirVeiculo', cdveiculo: cdveiculo},
                            success: function (data) {
                                abrirAvisos('-success', "Excluido com sucesso", "");
                                setTimeout(function () {
                                    location.reload();
                                }, 2000);
                            }
                        })
                    }
                });
            }

            function Gravar() {
                $("#btSalvar").removeAttr('href');
                $(".text-field").removeClass('-error');
                form = document.dados;
                flerro = 0;
                fecharAvisos();
                if (form.txnome_nomefantasia.value == "") {
                    flerro = 1;
                    $("#txnome_nomefantasia").addClass('-error')
                }
                if (form.txtipo.value == 'pj') {
                    if (form.txrazao.value == "") {
                        flerro = 1;
                        $("#txrazao").addClass('-error')
                    }
                    if (form.txresponsavel.value == "") {
                        flerro = 1;
                        $("#txresponsavel").addClass('-error')
                    }
                } else {
                    if (form.txrg.value == "") {
                        flerro = 1;
                        $("#txrg").addClass('-error')
                    }
                }
                if (form.txparceiro.value == "") {
                    flerro = 1;
                    $("#txparceiro").addClass('-error')
                }
                if (form.txcpf_cnpj.value == "") {
                    flerro = 1;
                    $("#txcpf_cnpj").addClass('-error')
                }
                if (form.txendereco.value == "") {
                    flerro = 1;
                    $("#txendereco").addClass('-error')
                }
                if (form.txnumero.value == "") {
                    flerro = 1;
                    $("#txnumero").addClass('-error')
                }
                if (form.txcomplemento.value == "") {
                    flerro = 1;
                    $("#txcomplemento").addClass('-error')
                }
                if (form.txtelefone.value == "") {
                    flerro = 1;
                    $("#txtelefone").addClass('-error')
                }
                if (form.txcelular.value == "") {
                    flerro = 1;
                    $("#txcelular").addClass('-error')
                }
                if (flerro) {
                    abrirAvisos('-error', 'Preencha todos os campos marcados.');
                    $("#btSalvar").attr('href', 'javascript:Gravar()');
                    return false;
                } else {
                    f_data = new FormData(form);
                    $.ajax({
                        type: "POST",
                        url: "/painel/acao.php",
                        data: f_data,
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            if ($.trim(data) == 'atualizar') {
                                abrirAvisos('-success', "Atualizado com sucesso", "");
                                setTimeout(function () {
                                    location.href = "/painel/parceiros";
                                }, 2000);
                            } else {
                                if (data > 0) {
                                    location.href = "/painel/novo-parceiro/" + data + "/sucesso";
                                } else {
                                    abrirAvisos('-error', 'Aconteceu um erro, tente mais tarde.');
                                    $("#btSalvar").attr('href', 'javascript:Gravar()');
                                }
                            }
                        }
                    })
                }
            }

            function selecionaTipoPessoa(txtipo) {
                if (txtipo == 1) {
                    $("label#txnome_nomefantasia").html('Nome Fantasia<span class="typo-color-error">*</span>');
                    $(".camposPJ").removeClass('-hide');
                    $("label#txcpf_cnpj").html('CNPJ<span class="typo-color-error">*</span>');
                    $("label#txrg").addClass('-hide');
                } else {
                    $("label#txnome_nomefantasia").html('Nome<span class="typo-color-error">*</span>');
                    $(".camposPJ").addClass('-hide');
                    $("label#txcpf_cnpj").html('CPF<span class="typo-color-error">*</span>');
                    $("label#txrg").removeClass('-hide');
                }
            }

        </script>
    </head>
    <body>
        <div class="side-bar-overlay js-menu-close"></div>
        <main class="main-box">
            <?php
            include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/header_painel.php';
            include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/menu_painel.php';
            ?>
            <div class="content-main">
                <div class="inner">
                    <div class="title container-block">
                        <h4 class="typo-color-light-text-primary typo-display-1 _d-block">Parceiros</h4>
                        <div class="breadcrumbs-container _mt-sm">
                            <a href="/painel/parceiros" class="link typo-body-2 typo-color-light-text-primary">Parceiros</a>
                            <a class="link typo-body-2 typo-color-light-text-primary">Cadastro / Edição</a>
                        </div>
                    </div>
                    <div class="grid-row -gutter-xl">
                        <div class="col-12">
                            <div class="card-box"> 
                                <div class="body">
                                    <form name="dados" id="dados" method="POST" action="javascript:Gravar()" class="grid-row -gutter-xl">
                                        <input type="hidden" id="acao" name="acao" value="gravarParceiro"/>
                                        <input type="hidden" id="cdparceiro" name="cdparceiro" value="<?= $dado['cdparceiro'] ?>"/>
                                        <div id="cxAviso" class="col-12 -hide cxAviso">
                                            <div id="divAviso" class="alert-box -success _d-flex _al-center">
                                                <i class="svg-icon -md -white _mr-sm" id="iconAviso">
                                                    <svg><use xlink:href="#icon-alert-success" /></svg>
                                                </i>
                                                <span class="text" id="spanAviso">Sua mensagem enviada com sucesso!</span>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <label class="typo-caption typo-color-black-40 _d-block _mb-sm _upper">Tipo de Parceiro</label>
                                            <div class="text-field-ctrl -inline">

                                                <label class="item" onclick="selecionaTipoPessoa(1)">
                                                    <span class="input-switch _mr-xs">
                                                        <input type="radio" name="txtipo" value="pj" <?php if ($dado['txtipo'] == 'pj') echo 'checked'; ?>>
                                                        <div class="box">
                                                            <svg class="outline"><use xlink:href="#icon-radio-outline" /></svg>
                                                            <svg class="solid"><use xlink:href="#icon-radio-solid" /></svg>
                                                        </div>
                                                    </span>
                                                    <span class="text">Pessoa Juridica</span>
                                                </label>

                                                <label class="item" onclick="selecionaTipoPessoa(2)">
                                                    <span class="input-switch _mr-xs">
                                                        <input type="radio" name="txtipo" value="pf" <?php if ($dado['txtipo'] == 'pj') echo 'checked'; ?>>
                                                        <div class="box">
                                                            <svg class="outline"><use xlink:href="#icon-radio-outline" /></svg>
                                                            <svg class="solid"><use xlink:href="#icon-radio-solid" /></svg>
                                                        </div>
                                                    </span>
                                                    <span class="text">Pessoa Fisica</span>
                                                </label>

                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper">Parceiro (Apelido)<span class="typo-color-error">*</span></label>
                                            <input type="text" name="txparceiro" value="<?= $dado['txparceiro'] ?>" id="txparceiro" class="text-field ">
                                        </div>

                                        <div class="col-md-4">
                                            <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper" id="txnome_nomefantasia">Nome Fantasia<span class="typo-color-error">*</span></label>
                                            <input type="text" name="txnome_nomefantasia" value="<?= $dado['txnome_nomefantasia'] ?>"   id="txnome_nomefantasia"  class="text-field ">
                                        </div>

                                        <div class="col-md-4 camposPJ">
                                            <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper" id="txrazao">Razão Social<span class="typo-color-error">*</span></label>
                                            <input type="text" name="txrazao"  id="txrazao" value="<?= $dado['txrazao'] ?>"   class="text-field ">
                                        </div>

                                        <div class="col-md-4">
                                            <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper" id="txcpf_cnpj">CNPJ<span class="typo-color-error">*</span></label>
                                            <input type="tel" name="txcpf_cnpj"  id="txcpf_cnpj"  value="<?= $dado['txcpf_cnpj'] ?>"  class="text-field ">
                                        </div>

                                        <div class="col-md-4  -hide">
                                            <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper" id="txrg">RG<span class="typo-color-error">*</span></label>
                                            <input type="tel" name="txrg"  id="txrg"  value="<?= $dado['txrg'] ?>" class="text-field ">
                                        </div>

                                        <div class="col-md-4">
                                            <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper" >Endereço<span class="typo-color-error">*</span></label>
                                            <input type="text" name="txendereco"  id="txendereco"  value="<?= $dado['txendereco'] ?>" class="text-field ">
                                        </div>

                                        <div class="col-md-4">
                                            <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper" >Número<span class="typo-color-error">*</span></label>
                                            <input type="text" name="txnumero"  id="txnumero"  value="<?= $dado['txnumero'] ?>" class="text-field ">
                                        </div>

                                        <div class="col-md-4">
                                            <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper" >Complemento<span class="typo-color-error">*</span></label>
                                            <input type="text" name="txcomplemento"  id="txcomplemento"  value="<?= $dado['txcomplemento'] ?>"  class="text-field ">
                                        </div>

                                        <div class="col-md-4">
                                            <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper" >Bairro<span class="typo-color-error">*</span></label>
                                            <input type="text" name="txbairro"  id="txbairro"  value="<?= $dado['txbairro'] ?>"  class="text-field ">
                                        </div>

                                        <div class="col-md-4">
                                            <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper">Estado</label>
                                            <select id="txestado" name="txestado" class="text-field">
                                                <option>Selecione um estado</option> 
                                            </select>
                                        </div>

                                        <div class="col-md-4">
                                            <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper">Cidade</label>
                                            <select id="txcidade" name="txcidade" class="text-field">
                                                <option>Escolha o estado primeiro</option> 
                                            </select>
                                        </div>

                                        <div class="col-md-4">
                                            <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper" >Telefone<span class="typo-color-error">*</span></label>
                                            <input type="tel" name="txtelefone"  id="txtelefone"  value="<?= $dado['txtelefone'] ?>"  class="text-field ">
                                        </div>

                                        <div class="col-md-4">
                                            <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper" >Celular<span class="typo-color-error">*</span></label>
                                            <input type="tel" name="txcelular"  id="txcelular"  value="<?= $dado['txcelular'] ?>"  class="text-field ">
                                        </div>

                                        <div class="col-md-4 camposPJ">
                                            <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper" id="txresponsavel">Responsável<span class="typo-color-error">*</span></label>
                                            <input type="text" name="txresponsavel"  id="txresponsavel"  value="<?= $dado['txresponsavel'] ?>"  class="text-field ">
                                        </div> 
                                    </form>
                                    <?php
                                    if ($_GET['codigo']) {
                                        $veiculos = retornaTodosParceiroVeiculo($_GET['codigo']);
                                        ?>
                                        <div class="card-box  _mt-xl">
                                            <div class="header _d-flex">
                                                <span class="typo-title typo-fw-regular typo-color-dark-text-secondary _d-block _fg-1">Veiculos</span>
                                                <div class="_fg-0">
                                                    <a href="/painel/novo-veiculo/<?= $_GET['codigo'] ?>" class="button-base -outline">Novo veiculo</a>
                                                </div>
                                            </div>
                                            <?php
                                            if ($veiculos) {
                                                ?>
                                                <div class="body">
                                                    <div class="grid-row -gutter-zero grid-list">
                                                        <!-- header grid -->
                                                        <div class="col-12 header ifTwoIconRight"> 
                                                            <div class="grid-row -gutter-sm">
                                                                <div class="col-6">
                                                                    <span class="headerText typo-body-2 typo-fw-bold typo-color-dark-text-primary _upper">Modelo</span>
                                                                </div>
                                                                <div class="col-6">
                                                                    <span class="headerText typo-body-2 typo-fw-bold typo-color-dark-text-primary _upper">Placa</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php foreach ($veiculos as $veiculo) { ?>  
                                                            <div class="col-12 row ifTwoIconRight">

                                                                <div class="grid-row -gutter-sm">
                                                                    <div class="col-6">
                                                                        <span class="typo-body-2 typo-color-dark-text-secondary"><?= $veiculo['txparceiroveiculomodelo'] ?></span>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <span class="typo-body-2 typo-color-dark-text-secondary"><?= $veiculo['txplaca'] ?></span>
                                                                    </div>
                                                                </div>

                                                                <div class="boxIconsRight _d-flex">
                                                                    <a href="/painel/novo-veiculo/<?= $_GET['codigo'] ?>-<?= $veiculo['cdparceiroveiculo'] ?>" class="iconAction -if-hover _inline-flex _al-center _jc-center">
                                                                        <i class="svg-icon -md">
                                                                            <svg><use xlink:href="#icon-edit"></use></svg>
                                                                        </i>
                                                                    </a>

                                                                    <a onclick="excluirVeiculo(<?= $veiculo['cdparceiroveiculo'] ?>)" class="iconAction -if-hover _inline-flex _al-center _jc-center">
                                                                        <i class="svg-icon -md">
                                                                            <svg><use xlink:href="#icon-delete"></use></svg>
                                                                        </i>
                                                                    </a>
                                                                </div>

                                                            </div>

                                                        <?php } ?>

                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>

                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="footer _d-flex">
                                    <div class="_fg-1">
                                        <a href="javascript:window.history.back();" class="button-base -outline">Voltar</a>
                                    </div>
                                    <div class="_fs-0">
                                        <a href="javascript:Gravar()" id="btSalvar" class="button-base">Salvar</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </main>
    </body>
</html>
