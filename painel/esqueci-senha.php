<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/controllers.php';
$tipoPage = "logar";
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <?php
        include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/head_painel.php';
        include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/stylesheets_painel.php';
        include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/scripts_painel.php';
        ?>
        <script>

            function Esqueci() {
                $(".text-field").removeClass('-error');
                fecharAvisos();
                flerro = 0;
                if ($("#txemail").val() == "") {
                    $("#txemail").addClass('-error')
                    flerro = 1;
                }
                if (flerro) {
                    abrirAvisos('-error', 'Preencha todos os campos marcados.');
                    return false;
                } else {
                    $.ajax({
                        type: "POST",
                        url: "/painel/ajax.php",
                        data: {acao: 'esqueci-senha', txemail: $("#txemail").val()},
                        success: function (data) {
                            if ($.trim(data) == 'sucess') {
                                $("#txemail").val('')
                                abrirAvisos('-success', 'Senha enviada com sucesso!');
                            } else {
                                abrirAvisos('-error', 'Nenhum usuario encontrado!');
                            }
                        }
                    })
                }
            }

        </script>
    </head>
    <body>

        <div class="login-box _d-flex _al-center _jc-center">
            <div class="inner">
                <h4 class="typo-display-1 typo-color-dark-text-primary _d-block _text-center _upper _mb-md">logo</h4>
                <div class="card-box _center-block">
                    <div class="body">
                        <div class="grid-row -gutter-lg">
                            <div id="cxAviso" class="col-12 -hide cxAviso">
                                <div id="divAviso" class="alert-box -success _d-flex _al-center">
                                    <i class="svg-icon -md -white _mr-sm" id="iconAviso">
                                        <svg><use xlink:href="#icon-alert-success" /></svg>
                                    </i>
                                    <span class="text" id="spanAviso">Sua mensagem enviada com sucesso!</span>
                                </div>
                            </div> 

                            <div class="col-12">
                                <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper">Uma nova senha será enviada ao e-mail cadastrado</label> 
                            </div>

                            <div class="col-12">
                                <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper">E-mail</label>
                                <input type="text" id="txemail" name="txemail" class="text-field">
                            </div>

                            <div class="col-12">
                                <a href="javascript:Esqueci();" class="button-base _w-100 _text-center">Enviar Senha</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>