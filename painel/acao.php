<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/controllers.php';

$acao = isset($_POST['acao']) ? $_POST['acao'] : $_GET['acao'];

$_POST = utf8_decode_array($_POST);
switch ($acao) {
    case 'login':
        $login = retornaUsuarioLogin($_POST['txusuario'], $_POST['txsenha']);
        if ($login) {
            $_SESSION['cdusuario'] = $login['cdusuario'];
            $_SESSION['txnome'] = $login['txnome'];
            if (!retornaUsuarioLogado($login['cdusuario'])) {
                insertUsuarioLogado($login['cdusuario'], md5($login['cdusuario']));
            }
            setcookie("codigo", md5($login['cdusuario']), time() + (3600 * 360));
            echo 'sucess';
        } else {
            echo 'erro';
        }
        exit;
        break;
    case 'gravarParceiro':
        if ($_POST['cdparceiro']) {
            updateParceiro($_POST['cdparceiro'], $_POST['txparceiro'], $_POST['txnome_nomefantasia'], $_POST['txrazao'], $_POST['txtipo'], $_POST['txcpf_cnpj'], $_POST['txrg'], $_POST['txendereco'], $_POST['txnumero'], $_POST['txcomplemento'], $_POST['txbairro'], $_POST['txcidade'], $_POST['txestado'], $_POST['txresponsavel'], $_POST['flsituacao'], $_POST['txtelefone'], $_POST['txcelular']);
            echo 'atualizar';
        } else {
            $idParceiro = insertParceiro($_POST['txparceiro'], $_POST['txnome_nomefantasia'], $_POST['txrazao'], $_POST['txtipo'], $_POST['txcpf_cnpj'], $_POST['txrg'], $_POST['txendereco'], $_POST['txnumero'], $_POST['txcomplemento'], $_POST['txbairro'], $_POST['txcidade'], $_POST['txestado'], $_POST['txresponsavel'], 1, 1, $_POST['txtelefone'], $_POST['txcelular']);
            echo $idParceiro;
        }
        exit;
        break;
    case 'excluirParceiro':
        if ($_POST['cdparceiro']) {
            deleteParceiro($_POST['cdparceiro']);
        } else {
            if ($_POST['arrParceiro']) {
                foreach ($_POST['arrParceiro'] as $cdparceiro) {
                    deleteParceiro($cdparceiro);
                }
            }
        }
        exit;
        break;
    case 'gravarVeiculo':
        if ($_POST['cdveiculo']) {
            updateParceiroVeiculo($_POST['cdveiculo'], $_POST['cdparceiroveiculomodelo'], $_POST['cdparceiroveiculotipo'], $_POST['txplaca'], $_POST['txrenavam'], $_POST['txmarca_modelo'], $_POST['txanofabricacao'], $_POST['txanomodelo'], $_POST['txcor'], $_POST['vlcapacidade'], $_POST['txdocumento'], $_POST['txfoto1'], $_POST['txfoto2'], $_POST['txfoto3'], $_POST['txfoto4'], $_POST['flsituacao']);
        } else {
            $idVeiculo = insertParceiroVeiculo($_POST['cdparceiroveiculomodelo'], $_POST['cdparceiroveiculotipo'], $_POST['cdparceiro'], $_POST['txplaca'], $_POST['txrenavam'], $_POST['txmarca_modelo'], $_POST['txanofabricacao'], $_POST['txanomodelo'], $_POST['txcor'], $_POST['vlcapacidade'], $_POST['txdocumento'], $_POST['txfoto1'], $_POST['txfoto2'], $_POST['txfoto3'], $_POST['txfoto4'], $_POST['flsituacao']);
            echo $idVeiculo;
        }
        exit;
        break;
    case 'excluirVeiculo':
        if ($_POST['cdveiculo']) {
            deleteParceiroVeiculo($_POST['cdveiculo']);
        } else {
            if ($_POST['arrVeiculo']) {
                foreach ($_POST['arrVeiculo'] as $cdveiculo) {
                    deleteParceiroVeiculo($cdveiculo);
                }
            }
        }
        exit;
        break;
    case 'gravarveiculomodelo':
        if ($_POST['cdveiculomodelo']) {
            updateParceiroveiculomodelo($_POST['cdveiculomodelo'], $_POST['txparceiroveiculomodelo']);
        } else {
            $idVeiculo = insertParceiroveiculomodelo($_POST['txparceiroveiculomodelo'], $_POST['flsituacao']);
            echo $idVeiculo;
        }
        exit;
        break;
    case 'excluirveiculomodelo':
        if ($_POST['cdveiculomodelo']) {
            deleteParceiroveiculomodelo($_POST['cdveiculomodelo']);
        } else {
            if ($_POST['arrveiculomodelo']) {
                foreach ($_POST['arrveiculomodelo'] as $cdveiculomodelo) {
                    deleteParceiroveiculomodelo($cdveiculomodelo);
                }
            }
        }
        exit;
        break;

    case 'gravarveiculotipo':
        if ($_POST['cdveiculotipo']) {
            updateParceiroveiculotipo($_POST['cdveiculotipo'], $_POST['txparceiroveiculotipo']);
        } else {
            $idVeiculo = insertParceiroveiculotipo($_POST['txparceiroveiculotipo'], $_POST['flsituacao']);
            echo $idVeiculo;
        }
        exit;
        break;
    case 'excluirveiculotipo':
        if ($_POST['cdveiculotipo']) {
            deleteParceiroveiculotipo($_POST['cdveiculotipo']);
        } else {
            if ($_POST['arrveiculotipo']) {
                foreach ($_POST['arrveiculotipo'] as $cdveiculotipo) {
                    deleteParceiroveiculotipo($cdveiculotipo);
                }
            }
        }
        exit;
        break;
    case 'gravarUsuario':
        if ($_POST['cdusuario']) {
            updateUsuarioupdateUsuario($_POST['cdusuario'], 2, $_POST['txusuario'], $_POST['txnome'], $_POST['txemail'], $_POST['txrg'], $_POST['txcpf'], $_POST['txtelefone'], $_POST['txcelular']);
        } else {
            $idUsuario = insertUsuario(2, $_POST['txusuario'], $_POST['txnome'], $_POST['txemail'], $_POST['txrg'], $_POST['txcpf'], $_POST['txtelefone'], $_POST['txcelular'], $_POST['txsenha']);
            echo $idUsuario;
        }
        exit;
        break;
    case 'excluirUsuario':
        if ($_POST['cdusuario']) {
            deleteUsuario($_POST['cdusuario']);
        } else {
            if ($_POST['arrUsuario']) {
                foreach ($_POST['arrUsuario'] as $cdusuario) {
                    deleteUsuario($cdusuario);
                }
            }
        }
        exit;
        break;

    case 'gravarpergunta':
        if ($_POST['cdpergunta']) {
            updatePergunta($_POST['cdpergunta'], $_POST['txpergunta']);
        } else {
            $idVeiculo = insertPergunta($_POST['txpergunta'], $_POST['flsituacao']);
            echo $idVeiculo;
        }
        exit;
        break;
    case 'excluirpergunta':
        if ($_POST['cdpergunta']) {
            deletePergunta($_POST['cdpergunta']);
        } else {
            if ($_POST['arrpergunta']) {
                foreach ($_POST['arrpergunta'] as $cdpergunta) {
                    deletePergunta($cdpergunta);
                }
            }
        }
        exit;
        break;
}
?>
