<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/controllers.php';
$tipoPage = 'usuario';
include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/session_painel.php';
if ($_GET['codigo']) {
    $dado = retornaUsuario($_GET['codigo']);
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <?php
        include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/head_painel.php';
        include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/stylesheets_painel.php';
        include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/scripts_painel.php';
        ?>
        <script>
            function Gravar() {
                $("#btSalvar").removeAttr('href');
                $(".text-field").removeClass('-error');
                form = document.dados;
                flerro = 0;
                fecharAvisos();
                if (form.txnome.value == "") {
                    flerro = 1;
                    $("#txnome").addClass('-error')
                }
                if (form.txemail.value == "") {
                    flerro = 1;
                    $("#txemail").addClass('-error')
                }
                if (!form.cdusuario.value) {
                    if (form.txsenha.value == "") {
                        flerro = 1;
                        $("#txsenha").addClass('-error')
                    } else {
                        if (form.txsenha.value != form.txconfirmacaosenha.value) {
                            flerro = 1;
                            $("#txsenha").addClass('-error')
                            $("#txconfirmacaosenha").addClass('-error')
                        }
                    }
                }
                if (flerro) {
                    abrirAvisos('-error', 'Preencha todos os campos marcados.');
                    $("#btSalvar").attr('href', 'javascript:Gravar()');
                    return false;
                } else {
                    f_data = new FormData(form);
                    $.ajax({
                        type: "POST",
                        url: "/painel/acao.php",
                        data: f_data,
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            abrirAvisos('-success', "Dados gravados com sucesso", "");
                            setTimeout(function () {
                                location.href = "/painel/usuarios";
                            }, 2000);
                        }
                    })
                }
            }

        </script> 
    </head>
    <body>
        <div class="side-bar-overlay js-menu-close"></div>
        <main class="main-box">
            <?php
            include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/header_painel.php';
            include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/menu_painel.php';
            ?>
            <div class="content-main">
                <div class="inner">
                    <div class="title container-block">
                        <h4 class="typo-color-light-text-primary typo-display-1 _d-block">Usuario</h4>
                        <div class="breadcrumbs-container _mt-sm">
                            <a href="/painel/usuarios" class="link typo-body-2 typo-color-light-text-primary">Usuarios</a>
                            <a class="link typo-body-2 typo-color-light-text-primary">Listagem</a>
                        </div>
                    </div>
                    <div class="grid-row -gutter-xl">
                        <div class="col-12">
                            <div class="card-box"> 
                                <div class="body">
                                    <form name="dados" id="dados" method="POST" action="javascript:Gravar()" class="grid-row -gutter-xl">
                                        <input type="hidden" id="acao" name="acao" value="gravarUsuario"/>
                                        <input type="hidden" id="cdusuario" name="cdusuario" value="<?= $dado['cdusuario'] ?>"/>
                                        <div id="cxAviso" class="col-12 -hide cxAviso">
                                            <div id="divAviso" class="alert-box -success _d-flex _al-center">
                                                <i class="svg-icon -md -white _mr-sm" id="iconAviso">
                                                    <svg><use xlink:href="#icon-alert-success" /></svg>
                                                </i>
                                                <span class="text" id="spanAviso">Sua mensagem enviada com sucesso!</span>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper">Nome<span class="typo-color-error">*</span></label>
                                            <input type="text" name="txnome" value="<?= $dado['txnome'] ?>" id="txnome" class="text-field ">
                                        </div>

                                        <div class="col-md-6">
                                            <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper">E-mail<span class="typo-color-error">*</span></label>
                                            <input type="email" name="txemail" value="<?= $dado['txemail'] ?>" id="txemail" class="text-field ">
                                        </div>
                                        <?php if (!$dado['cdusuario']) { ?>
                                            <div class="col-md-6">
                                                <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper">Senha<span class="typo-color-error">*</span></label>
                                                <input type="password" name="txsenha" value="<?= $dado['txsenha'] ?>" id="txsenha" class="text-field ">
                                            </div>

                                            <div class="col-md-6">
                                                <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper">Confirmação de Senha<span class="typo-color-error">*</span></label>
                                                <input type="password" name="txconfirmacaosenha" value="<?= $dado['txconfirmacaosenha'] ?>" id="txconfirmacaosenha" class="text-field ">
                                            </div>
                                        <?php } ?>
                                    </form>
                                </div>
                                <div class="footer _d-flex">
                                    <div class="_fg-1">
                                        <a href="javascript:window.history.back();" class="button-base -outline">Voltar</a>
                                    </div>
                                    <div class="_fs-0">
                                        <a href="javascript:Gravar()" id="btSalvar" class="button-base">Salvar</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </main>
    </body>
</html>
