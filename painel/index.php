<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/controllers.php';
$tipoPage = "logar";
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <?php
        include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/head_painel.php';
        include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/stylesheets_painel.php';
        include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/scripts_painel.php';
        ?>
        <script>
            function login() {
                $(".text-field").removeClass('-error');
                flerro = 0;
                fecharAvisos();
                if ($("#txusuario").val() == "") {
                    flerro = 1;
                    $("#txusuario").addClass('-error')
                }
                if ($("#txsenha").val() == "") {
                    flerro = 1;
                    $("#txsenha").addClass('-error')
                }
                if (flerro) {
                    abrirAvisos('-error', 'Preencha todos os campos marcados.');
                    return false;
                } else {
                    $.ajax({
                        type: "POST",
                        url: "/painel/ajax.php",
                        data: {acao: 'login', txusuario: $("#txusuario").val(), txsenha: $("#txsenha").val()},
                        success: function (data) {
                            if ($.trim(data) == 'sucess') {
                                location.href = "/painel/parceiros";
                            } else {
                                abrirAvisos('-error', 'Aconteceu um erro, tente mais tarde.');
                            }
                        }
                    })
                }
            }
        </script>
    </head>
    <body>

        <div class="login-box _d-flex _al-center _jc-center">
            <div class="inner">
                <h4 class="typo-display-1 typo-color-dark-text-primary _d-block _text-center _upper _mb-md">logo</h4>
                <div class="card-box _center-block">
                    <div class="body">
                        <div class="grid-row -gutter-lg">
                            <div id="cxAviso" class="col-12 -hide cxAviso">
                                <div id="divAviso" class="alert-box -success _d-flex _al-center">
                                    <i class="svg-icon -md -white _mr-sm" id="iconAviso">
                                        <svg><use xlink:href="#icon-alert-success" /></svg>
                                    </i>
                                    <span class="text" id="spanAviso">Sua mensagem enviada com sucesso!</span>
                                </div>
                            </div> 

                            <div class="col-12">
                                <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper">E-mail</label>
                                <input type="text" id="txusuario" name="txusuario" class="text-field">
                            </div>

                            <div class="col-12">
                                <label class="typo-caption typo-color-black-40 _d-block _mb-xs _upper">Senha</label>
                                <input type="password" id="txsenha" name="txsenha"  class="text-field">

                                <a href="/painel/esqueci-senha" class="typo-body-2 typo-color-black-40 _mt-sm _d-block _text-right _link-forgot-pass">Esqueci minha senha</a>
                            </div>

                            <div class="col-12">
                                <a href="javascript:login();" class="button-base _w-100 _text-center">Entrar</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>