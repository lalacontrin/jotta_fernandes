<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/controllers.php';
$tipoPage = 'pergunta';
include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/session_painel.php';
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <?php
        include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/head_painel.php';
        include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/stylesheets_painel.php';
        include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/scripts_painel.php';
        ?>

        <script>
            function excluirPergunta(cdpergunta) {
                Swal.fire({
                    title: 'Deseja remover este registro?',
                    text: "Você não poderá reverter isso!",
                    type: 'warning',
                    showCancelButton: true,
                    buttonsStyling: false,
                    confirmButtonText: 'Sim, remover!',
                    cancelButtonText: "Cancelar",
                    customClass: {
                        confirmButton: 'button-base',
                        cancelButton: 'button-base -cancel'
                    },
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: "POST",
                            url: "/painel/acao.php",
                            data: {acao: 'excluirPergunta', cdpergunta: cdpergunta},
                            success: function (data) {
                                abrirAvisos('-success', "Excluido com sucesso", "");
                                setTimeout(function () {
                                    location.reload();
                                }, 2000);
                            }
                        })
                    }
                });
            }
        </script>
    </head>
    <body>
        <div class="side-bar-overlay js-menu-close"></div>
        <main class="main-box">
            <?php
            include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/header_painel.php';
            include_once $_SERVER['DOCUMENT_ROOT'] . '/includes/menu_painel.php';
            ?>
            <div class="content-main">
                <div class="inner">
                    <div class="title container-block">
                        <h4 class="typo-color-light-text-primary typo-display-1 _d-block">Perguntas</h4>
                        <div class="breadcrumbs-container _mt-sm">
                            <a href="/painel/perguntas" class="link typo-body-2 typo-color-light-text-primary">Perguntas</a>
                            <a class="link typo-body-2 typo-color-light-text-primary">Listagem</a>
                        </div>
                    </div>
                    <div class="grid-row -gutter-xl">
                        <div id="cxAviso" class="col-12 -hide cxAviso">
                            <div id="divAviso" class="alert-box -success _d-flex _al-center">
                                <i class="svg-icon -md -white _mr-sm" id="iconAviso">
                                    <svg><use xlink:href="#icon-alert-success" /></svg>
                                </i>
                                <span class="text" id="spanAviso">Sua mensagem enviada com sucesso!</span>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="card-box">
                                <div class="header _d-flex">
                                    <span class="typo-title typo-fw-regular typo-color-dark-text-secondary _d-block _fg-1">Perguntas</span>
                                    <div class="_fg-0">
                                        <a href="/painel/nova-pergunta" class="button-base -outline">Nova Pergunta</a>
                                    </div>
                                </div>
                                <?php $perguntas = retornaTodosPergunta(); ?>
                                <div class="body">
                                    <div class="grid-row -gutter-zero grid-list">
                                        <!-- header grid -->
                                        <div class="col-12 header  ifTwoIconRight"> 
                                            <div class="grid-row -gutter-sm">
                                                <div class="col-12">
                                                    <span class="headerText typo-body-2 typo-fw-bold typo-color-dark-text-primary _upper">Perguntas</span>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        if ($perguntas) {
                                            foreach ($perguntas as $p) {
                                                ?>  
                                                <div class="col-12 row ifTwoIconRight"> 

                                                    <div class="grid-row -gutter-sm">
                                                        <div class="col-12">
                                                            <span class="typo-body-2 typo-color-dark-text-secondary"><?= $p['txpergunta'] ?></span>
                                                        </div>
                                                    </div>

                                                    <div class="boxIconsRight _d-flex">
                                                        <a href="/painel/nova-pergunta/<?= $p['cdpergunta'] ?>" class="iconAction -if-hover _inline-flex _al-center _jc-center">
                                                            <i class="svg-icon -md">
                                                                <svg><use xlink:href="#icon-edit"></use></svg>
                                                            </i>
                                                        </a>

                                                        <a onclick="excluirPergunta(<?= $p['cdpergunta'] ?>)" class="iconAction -if-hover _inline-flex _al-center _jc-center">
                                                            <i class="svg-icon -md">
                                                                <svg><use xlink:href="#icon-delete"></use></svg>
                                                            </i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                        } else {
                                            
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </body>
</html>


