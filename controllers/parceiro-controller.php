<?php

include_once $_SERVER['DOCUMENT_ROOT']. '/models/parceiro.php';

function insertParceiro($txparceiro, $txnome_nomefantasia, $txrazao, $txtipo, $txcpf_cnpj, $txrg, $txendereco, $txnumero, $txcomplemento, $txbairro, $txcidade, $txestado, $txresponsavel, $flpainel, $flsituacao, $txtelefone, $txcelular) {
    $parceiro = new parceiro();
    $result = $parceiro->insertParceiro($txparceiro, $txnome_nomefantasia, $txrazao, $txtipo, $txcpf_cnpj, $txrg, $txendereco, $txnumero, $txcomplemento, $txbairro, $txcidade, $txestado, $txresponsavel, $flpainel, $flsituacao, $txtelefone, $txcelular);
    return $result;
}

function updateParceiro($cdparceiro, $txparceiro, $txnome_nomefantasia, $txrazao, $txtipo, $txcpf_cnpj, $txrg, $txendereco, $txnumero, $txcomplemento, $txbairro, $txcidade, $txestado, $txresponsavel, $flsituacao, $txtelefone, $txcelular) {
    $parceiro = new parceiro();
    $result = $parceiro->updateParceiro($cdparceiro, $txparceiro, $txnome_nomefantasia, $txrazao, $txtipo, $txcpf_cnpj, $txrg, $txendereco, $txnumero, $txcomplemento, $txbairro, $txcidade, $txestado, $txresponsavel, $flsituacao, $txtelefone, $txcelular);
    return $result;
}

function updateSituacaoParceiro($cdparceiro, $flsituacao) {
    $parceiro = new parceiro();
    $result = $parceiro->updateSituacaoParceiro($cdparceiro, $flsituacao);
    return $result;
}

function deleteParceiro($cdparceiro) {
    $parceiro = new parceiro();
    $result = $parceiro->deleteParceiro($cdparceiro);
    return $result;
}

function retornaParceiro($cdparceiro) {
    $parceiro = new parceiro();
    $result = $parceiro->retornaParceiro($cdparceiro);
    return $result;
}

function retornaQuantidadesParceiro($busca = null) {
    $sindico = new parceiro();
    $result = $sindico->retornaQuantidadesParceiro($busca);
    return $result;
}

function retornaTodosParceiro($busca = null, $tipo = null) {
    $sindico = new parceiro();
    $result = $sindico->retornaTodosParceiro($busca, $tipo);
    return $result;
}

/* * **********************parceiro dia*********************** */

function insertParceiroDia($cdparceiro, $cddia) {
    $sindico = new parceiro();
    $result = $sindico->insertParceiroDia($cdparceiro, $cddia);
    return $result;
}

function deleteTodosParceiroDia($cdparceiro) {
    $sindico = new parceiro();
    $result = $sindico->deleteTodosParceiroDia($cdparceiro);
    return $result;
}

function retornaParceiroDia($cdparceiro, $cddia) {
    $sindico = new parceiro();
    $result = $sindico->retornaParceiroDia($cdparceiro, $cddia);
    return $result;
}

/* * **********************parceiro Cidade Estado*********************** */

function insertParceiroEstadoCidade($cdparceiro, $cdestado, $cdestadocidade) {
    $sindico = new parceiro();
    $result = $sindico->insertParceiroEstadoCidade($cdparceiro, $cdestado, $cdestadocidade);
    return $result;
}

function deleteTodosParceiroEstadoCidade($cdparceiro) {
    $sindico = new parceiro();
    $result = $sindico->deleteTodosParceiroEstadoCidade($cdparceiro);
    return $result;
}

function retornaParceiroEstadoCidade($cdparceiro, $cdestado, $cdestadocidade) {
    $sindico = new parceiro();
    $result = $sindico->retornaParceiroEstadoCidade($cdparceiro, $cdestado, $cdestadocidade);
    return $result;
}

?>