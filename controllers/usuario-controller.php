<?php

include_once $_SERVER['DOCUMENT_ROOT']. '/models/usuario.php';

function insertUsuario($cdusuariotipo, $txusuario, $txnome, $txemail, $txrg, $txcpf, $txtelefone, $txcelular, $txsenha) {
    $usuario = new usuario();
    $result = $usuario->insertUsuario($cdusuariotipo, $txusuario, $txnome, $txemail, $txrg, $txcpf, $txtelefone, $txcelular, $txsenha);
    return $result;
}

function updateUsuario($cdusuario, $cdusuariotipo, $txusuario, $txnome, $txemail, $txrg, $txcpf, $txtelefone, $txcelular) {
    $usuario = new usuario();
    $result = $usuario->updateUsuario($cdusuario, $cdusuariotipo, $txusuario, $txnome, $txemail, $txrg, $txcpf, $txtelefone, $txcelular);
    return $result;
}

function updateSituacaoUsuario($cdusuario, $flsituacao) {
    $usuario = new usuario();
    $result = $usuario->updateSituacaoUsuario($cdusuario, $flsituacao);
    return $result;
}

function updateSenhaUsuario($cdusuario, $txsenha) {
    $usuario = new usuario();
    $result = $usuario->updateSenhaUsuario($cdusuario, $txsenha);
    return $result;
}

function deleteUsuario($cdusuario) {
    $usuario = new usuario();
    $result = $usuario->deleteUsuario($cdusuario);
    return $result;
}

function retornaUsuario($cdusuario) {
    $usuario = new usuario();
    $result = $usuario->retornaUsuario($cdusuario);
    return $result;
}

function retornaUsuarioEmail($txemail) {
    $usuario = new usuario();
    $result = $usuario->retornaUsuarioEmail($txemail);
    return $result;
}

function retornaUsuarioLogin($txemail, $txsenha) {
    $usuario = new usuario();
    $result = $usuario->retornaUsuarioLogin($txemail, $txsenha);
    return $result;
}

function retornaqtdeUsuario($filtro = null) {
    $usuario = new usuario();
    $result = $usuario->retornaqtdeUsuario($filtro);
    return $result;
}

function listaUsuario($pg = null, $qtde = null, $filtro = null) {
    $usuario = new usuario();
    $result = $usuario->listaUsuario($pg, $qtde, $filtro);
    return $result;
}

/* * ************Usuario Logado************** */

function insertUsuarioLogado($cdusuario, $codigo) {
    $usuario = new usuario();
    $result = $usuario->insertUsuarioLogado($cdusuario, $codigo);
    return $result;
}

function updateUsuarioLogado($cdlogado, $cdusuario, $codigo) {
    $usuario = new usuario();
    $result = $usuario->updateUsuarioLogado($cdlogado, $cdusuario, $codigo);
    return $result;
}

function deleteUsuarioLogado($cdusuario) {
    $usuario = new usuario();
    $result = $usuario->deleteUsuarioLogado($cdusuario);
    return $result;
}

function retornaUsuarioLogado($cdusuario) {
    $usuario = new usuario();
    $result = $usuario->retornaUsuarioLogado($cdusuario);
    return $result;
}

function retornaUsuarioLogadoCodigo($codigo) {
    $usuario = new usuario();
    $result = $usuario->retornaUsuarioLogadoCodigo($codigo);
    return $result;
}
