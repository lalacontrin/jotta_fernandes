<?php

include_once $_SERVER['DOCUMENT_ROOT']. '/models/parceiromotorista.php';

function insertParceiroMotorista($cdparceiro, $txnome, $txrg, $txcpf, $txhabilitacao, $txdocumento, $txfoto1, $flsituacao) {
    $parceiromotorista = new parceiromotorista();
    $result = $parceiromotorista->insertParceiroMotorista($cdparceiro, $txnome, $txrg, $txcpf, $txhabilitacao, $txdocumento, $txfoto1, $flsituacao);
    return $result;
}

function updateParceiroMotorista($cdparceiromotorista, $txnome, $txrg, $txcpf, $txhabilitacao, $txdocumento, $txfoto1, $flsituacao) {
    $parceiromotorista = new parceiromotorista();
    $result = $parceiromotorista->updateParceiroMotorista($cdparceiromotorista, $txnome, $txrg, $txcpf, $txhabilitacao, $txdocumento, $txfoto1, $flsituacao);
    return $result;
}

function updateSituacaoParceiroMotorista($cdparceiromotorista, $flsituacao) {
    $parceiromotorista = new parceiromotorista();
    $result = $parceiromotorista->updateSituacaoParceiroMotorista($cdparceiromotorista, $flsituacao);
    return $result;
}

function deleteParceiroMotorista($cdparceiromotorista) {
    $parceiromotorista = new parceiromotorista();
    $result = $parceiromotorista->deleteParceiroMotorista($cdparceiromotorista);
    return $result;
}

function retornaParceiroMotorista($cdparceiromotorista) {
    $parceiromotorista = new parceiromotorista();
    $result = $parceiromotorista->retornaParceiroMotorista($cdparceiromotorista);
    return $result;
}

function retornaQuantidadesParceiroMotorista($busca = null) {
    $sindico = new parceiromotorista();
    $result = $sindico->retornaQuantidadesParceiroMotorista($busca);
    return $result;
}

function retornaTodosParceiroMotorista($busca = null, $tipo = null) {
    $sindico = new parceiromotorista();
    $result = $sindico->retornaTodosParceiroMotorista($busca, $tipo);
    return $result;
}
?>