<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/models/parceiroveiculotipo.php';
 
function insertParceiroveiculotipo($txparceiroveiculotipo, $flsituacao) {
    $parceiroveiculotipo = new parceiroveiculotipo();
    $result = $parceiroveiculotipo->insertParceiroveiculotipo($txparceiroveiculotipo, $flsituacao);
    return $result;
}

function updateParceiroveiculotipo($cdparceiroveiculotipo, $txparceiroveiculotipo, $flsituacao) {
    $parceiroveiculotipo = new parceiroveiculotipo();
    $result = $parceiroveiculotipo->updateParceiroveiculotipo($cdparceiroveiculotipo, $txparceiroveiculotipo, $flsituacao);
    return $result;
}

function updateSituacaoParceiroveiculotipo($cdparceiroveiculotipo, $flsituacao) {
    $parceiroveiculotipo = new parceiroveiculotipo();
    $result = $parceiroveiculotipo->updateSituacaoParceiroveiculotipo($cdparceiroveiculotipo, $flsituacao);
    return $result;
}

function deleteParceiroveiculotipo($cdparceiroveiculotipo) {
    $parceiroveiculotipo = new parceiroveiculotipo();
    $result = $parceiroveiculotipo->deleteParceiroveiculotipo($cdparceiroveiculotipo);
    return $result;
}

function retornaParceiroveiculotipo($cdparceiroveiculotipo) {
    $parceiroveiculotipo = new parceiroveiculotipo();
    $result = $parceiroveiculotipo->retornaParceiroveiculotipo($cdparceiroveiculotipo);
    return $result;
}

function retornaQuantidadesParceiroveiculotipo($busca = null) {
    $sindico = new parceiroveiculotipo();
    $result = $sindico->retornaQuantidadesParceiroveiculotipo($busca);
    return $result;
}

function retornaTodosParceiroveiculotipo($busca = null, $tipo = null) {
    $sindico = new parceiroveiculotipo();
    $result = $sindico->retornaTodosParceiroveiculotipo($busca, $tipo);
    return $result;
}


?>