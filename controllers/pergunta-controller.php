<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/models/pergunta.php';

function insertPergunta($txpergunta, $fltipo, $flsituacao) {
    $pergunta = new pergunta();
    $result = $pergunta->insertPergunta($txpergunta, $fltipo, $flsituacao);
    return $result;
}

function updatePergunta($cdpergunta, $txpergunta, $fltipo, $flsituacao) {
    $pergunta = new pergunta();
    $result = $pergunta->updatePergunta($cdpergunta, $txpergunta, $fltipo, $flsituacao);
    return $result;
}

function updateSituacaoPergunta($cdpergunta, $flsituacao) {
    $pergunta = new pergunta();
    $result = $pergunta->updateSituacaoPergunta($cdpergunta, $flsituacao);
    return $result;
}

function deletePergunta($cdpergunta) {
    $pergunta = new pergunta();
    $result = $pergunta->deletePergunta($cdpergunta);
    return $result;
}

function retornaPergunta($cdpergunta) {
    $pergunta = new pergunta();
    $result = $pergunta->retornaPergunta($cdpergunta);
    return $result;
}

function retornaQuantidadesPergunta($busca = null) {
    $sindico = new pergunta();
    $result = $sindico->retornaQuantidadesPergunta($busca);
    return $result;
}

function retornaTodosPergunta($busca = null, $tipo = null) {
    $sindico = new pergunta();
    $result = $sindico->retornaTodosPergunta($busca, $tipo);
    return $result;
}

function insertPerguntaRespostas($cdpergunta, $txperguntarespostas, $fltipo, $flsituacao) {
    $sindico = new pergunta();
    $result = $sindico->insertPerguntaRespostas($cdpergunta, $txperguntarespostas, $fltipo, $flsituacao);
    return $result;
}

function updatePerguntaRespostas($cdperguntarespostas, $cdpergunta, $txperguntarespostas, $fltipo, $flsituacao) {
    $sindico = new pergunta();
    $result = $sindico->updatePerguntaRespostas($cdperguntarespostas, $cdpergunta, $txperguntarespostas, $fltipo, $flsituacao);
    return $result;
}

function deletePerguntaRespostas($cdperguntarespostas) {
    $sindico = new pergunta();
    $result = $sindico->deletePerguntaRespostas($cdperguntarespostas);
    return $result;
}

function retornaPerguntaRespostas($cdperguntarespostas) {
    $sindico = new pergunta();
    $result = $sindico->retornaPerguntaRespostas($cdperguntarespostas);
    return $result;
}

function retornaTodosPerguntaRespostas($cdpergunta) {
    $sindico = new pergunta();
    $result = $sindico->retornaTodosPerguntaRespostas($cdpergunta);
    return $result;
}

?>