<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/models/parceiroveiculomodelo.php';
 
function insertParceiroveiculomodelo($txparceiroveiculomodelo, $flsituacao) {
    $parceiroveiculomodelo = new parceiroveiculomodelo();
    $result = $parceiroveiculomodelo->insertParceiroveiculomodelo($txparceiroveiculomodelo, $flsituacao);
    return $result;
}

function updateParceiroveiculomodelo($cdparceiroveiculomodelo, $txparceiroveiculomodelo, $flsituacao) {
    $parceiroveiculomodelo = new parceiroveiculomodelo();
    $result = $parceiroveiculomodelo->updateParceiroveiculomodelo($cdparceiroveiculomodelo, $txparceiroveiculomodelo, $flsituacao);
    return $result;
}

function updateSituacaoParceiroveiculomodelo($cdparceiroveiculomodelo, $flsituacao) {
    $parceiroveiculomodelo = new parceiroveiculomodelo();
    $result = $parceiroveiculomodelo->updateSituacaoParceiroveiculomodelo($cdparceiroveiculomodelo, $flsituacao);
    return $result;
}

function deleteParceiroveiculomodelo($cdparceiroveiculomodelo) {
    $parceiroveiculomodelo = new parceiroveiculomodelo();
    $result = $parceiroveiculomodelo->deleteParceiroveiculomodelo($cdparceiroveiculomodelo);
    return $result;
}

function retornaParceiroveiculomodelo($cdparceiroveiculomodelo) {
    $parceiroveiculomodelo = new parceiroveiculomodelo();
    $result = $parceiroveiculomodelo->retornaParceiroveiculomodelo($cdparceiroveiculomodelo);
    return $result;
}

function retornaQuantidadesParceiroveiculomodelo($busca = null) {
    $sindico = new parceiroveiculomodelo();
    $result = $sindico->retornaQuantidadesParceiroveiculomodelo($busca);
    return $result;
}

function retornaTodosParceiroveiculomodelo($busca = null, $tipo = null) {
    $sindico = new parceiroveiculomodelo();
    $result = $sindico->retornaTodosParceiroveiculomodelo($busca, $tipo);
    return $result;
}


?>