<?php

include_once $_SERVER['DOCUMENT_ROOT'] . '/models/parceiroveiculo.php';

function insertParceiroVeiculo($cdparceiroveiculomodelo, $cdparceiroveiculotipo, $cdparceiro, $txplaca, $txrenavam, $txmarca_modelo, $txanofabricacao, $txanomodelo, $txcor, $vlcapacidade, $txdocumento, $txfoto1, $txfoto2, $txfoto3, $txfoto4, $flsituacao) {
    $parceiroveiculo = new parceiroveiculo();
    $result = $parceiroveiculo->insertParceiroVeiculo($cdparceiroveiculomodelo, $cdparceiroveiculotipo, $cdparceiro, $txplaca, $txrenavam, $txmarca_modelo, $txanofabricacao, $txanomodelo, $txcor, $vlcapacidade, $txdocumento, $txfoto1, $txfoto2, $txfoto3, $txfoto4, $flsituacao);
    return $result;
}

function updateParceiroVeiculo($cdparceiroveiculo, $cdparceiroveiculomodelo, $cdparceiroveiculotipo, $txplaca, $txrenavam, $txmarca_modelo, $txanofabricacao, $txanomodelo, $txcor, $vlcapacidade, $txdocumento, $txfoto1, $txfoto2, $txfoto3, $txfoto4, $flsituacao) {
    $parceiroveiculo = new parceiroveiculo();
    $result = $parceiroveiculo->updateParceiroVeiculo($cdparceiroveiculo, $cdparceiroveiculomodelo, $cdparceiroveiculotipo, $txplaca, $txrenavam, $txmarca_modelo, $txanofabricacao, $txanomodelo, $txcor, $vlcapacidade, $txdocumento, $txfoto1, $txfoto2, $txfoto3, $txfoto4, $flsituacao);
    return $result;
}

function updateSituacaoParceiroVeiculo($cdparceiroveiculo, $flsituacao) {
    $parceiroveiculo = new parceiroveiculo();
    $result = $parceiroveiculo->updateSituacaoParceiroVeiculo($cdparceiroveiculo, $flsituacao);
    return $result;
}

function deleteParceiroVeiculo($cdparceiroveiculo) {
    $parceiroveiculo = new parceiroveiculo();
    $result = $parceiroveiculo->deleteParceiroVeiculo($cdparceiroveiculo);
    return $result;
}

function retornaParceiroVeiculo($cdparceiroveiculo) {
    $parceiroveiculo = new parceiroveiculo();
    $result = $parceiroveiculo->retornaParceiroVeiculo($cdparceiroveiculo);
    return $result;
}

function retornaQuantidadesParceiroVeiculo($busca = null) {
    $sindico = new parceiroveiculo();
    $result = $sindico->retornaQuantidadesParceiroVeiculo($busca);
    return $result;
}

function retornaTodosParceiroVeiculo($cdparceiro, $busca = null, $tipo = null) {
    $sindico = new parceiroveiculo();
    $result = $sindico->retornaTodosParceiroVeiculo($cdparceiro, $busca, $tipo);
    return $result;
}
 
?>