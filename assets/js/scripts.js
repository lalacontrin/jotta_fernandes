$('.js-menu-open').click(function () {
    $('.side-bar').addClass('-show');
    $('.side-bar-overlay').addClass('-show');
    $('body').addClass('-if-menu-open');
});

$('.js-menu-close').click(function () {
    $('.side-bar').removeClass('-show');
    $('.side-bar-overlay').removeClass('-show');
    $('body').removeClass('-if-menu-open');
});

function deleteData() {
    Swal.fire({
        title: 'Deseja remover este registro?',
        text: "Você não poderá reverter isso!",
        type: 'warning',
        showCancelButton: true,
        buttonsStyling: false,
        confirmButtonText: 'Sim, remover!',
        cancelButtonText: "Cancelar",
        customClass: {
            confirmButton: 'button-base',
            cancelButton: 'button-base -cancel'
        },
    }).then((result) => {
        if (result.value) {
            console.log('Ação aqui!');
        }
    });
}
;
